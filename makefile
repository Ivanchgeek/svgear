build:
	docker-compose rm -f
	docker-compose pull
	docker-compose build --no-cache

run: build
	docker-compose up -d

stop:
	docker-compose down -v

build-dev:
	docker-compose -f docker-compose.dev.yml rm -f
	docker-compose -f docker-compose.dev.yml pull
	docker-compose -f docker-compose.dev.yml build --no-cache

run-dev: build-dev
	docker-compose -f docker-compose.dev.yml up -d

stop-dev:
	docker-compose -f docker-compose.dev.yml down -v
