const express = require('express')
const fs = require('fs')

const argv = require('minimist')(process.argv.slice(2))

const app = express()
const port = parseInt(argv.port) || 5000
const toolsDir = __dirname + '/tools'

app.get('/svgear-tools.min.js', (req, res) => {
    res.sendFile( `${toolsDir}/svgear-tools.min.js`)
})

app.get('/presets/image', (req, res) => {
    res.sendFile( `${toolsDir}/image-preset.html`)
})

app.use('/presets', express.static(toolsDir))

app.get('/*', (req, res) => {
    const dir = __dirname + '/client/'
    let file = req.path || 'index.html'

    if (!fs.existsSync(dir + file)) file = 'index.html'

    res.sendFile(dir + file)
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
