const name = (new URLSearchParams(window.location.search)).get('name')

export function postMessage(cmd, payload) {
    parent.postMessage({cmd, payload, from: name}, '*')
}

function getFilterElement(filter) {
    const svg = document.createElement('div')
    svg.innerHTML = filter
    return svg.querySelector('svg')
}

export function handleMessages(extension) {
    window.addEventListener('message', (msg) => {
        // if (msg.origin !== 'http://localhost:8080') return
    
        const {cmd, payload} = msg.data
        switch (cmd) {
            case 'apply-filter':
                const {target: query, filter: filterText} = payload
                
                let filter
                try {
                    filter = getFilterElement(filterText)
                } catch {
                    postMessage('error', 'Bad filter text passed.')
                    break
                }

                const id = filter.querySelector('filter').id
    
                let target
                try {
                    target = document.querySelector(query)
                    target.style.filter = `url(#${id})`
                } catch {
                    postMessage('error', 'Wrong target query.')
                    break
                }
    
                const oldFilter = document.getElementById(id)
                if (oldFilter) oldFilter.parentNode.remove()
    
                target.parentNode.insertBefore(filter, target)
    
                break
            case 'reload-location':
                window.location.reload()
                break
            default:
                extension(cmd, payload)
        }
    })
    postMessage('tools-loaded')
}
