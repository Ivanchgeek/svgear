import { handleMessages } from './defaultHandler'

window.addEventListener('load', () => {
    handleMessages((cmd, payload) => {
        switch(cmd) {
            case 'set-image':
                const display = document.querySelector('#image-display')
                const bg = new Image()
                bg.src = payload
                display.style.backgroundImage = `url("${bg.src}")`
                break;
        }
    })
})
