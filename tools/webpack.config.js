const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: 'production',
    entry: {
        'svgear-tools': path.resolve(__dirname, './src/index.js'),
        'image-preset': path.resolve(__dirname, './src/imagePreset.js')
    },
    output: {
        path: path.resolve(__dirname, './build'),
        filename: '[name].min.js',
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, './presets/image-preset.html'),
            filename: 'image-preset.html',
            chunks: [
                'image-preset'
            ]
        }),
        new CleanWebpackPlugin(),
    ],
}
