import { createApp } from 'vue'
import App from './App.vue'
import MobilePlug from './MobilePlug.vue'
import { createPinia } from 'pinia'
import router from './router'
import i18n from './locale'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { 
    faPen, faXmark, faPlus, faChevronUp, faFlag, faCrosshairs, 
    faArrowRight, faArrowLeft, faMaximize, faMinimize, faHome,
    faQuestionCircle, faCheck, faSave, faCircleNodes, faGear,
    faRotateRight, faArrowsUpDown, faUpDownLeftRight,
    faFileExport, faGlobe,
} from '@fortawesome/free-solid-svg-icons'

library.add(faPen)
library.add(faXmark)
library.add(faCheck)
library.add(faPlus)
library.add(faChevronUp)
library.add(faFlag)
library.add(faCrosshairs)
library.add(faArrowRight)
library.add(faArrowLeft)
library.add(faMaximize)
library.add(faMinimize)
library.add(faHome)
library.add(faQuestionCircle)
library.add(faSave)
library.add(faCircleNodes)
library.add(faGear)
library.add(faRotateRight)
library.add(faArrowsUpDown)
library.add(faUpDownLeftRight)
library.add(faFileExport)
library.add(faGlobe)

function isMobile() {
    if (!(navigator && navigator.userAgent)) return false
    return (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent))
}

if (isMobile()) {
    createApp(MobilePlug).mount('#app')
} else {
    const app = createApp(App)

    app
        .use(createPinia())
        .use(router)
        .use(i18n)
        .component('font-awesome-icon', FontAwesomeIcon)
        .mount('#app')

    app.config.errorHandler = (err, _, info) => {
        console.error(err, info)
        router.push({name: 'UnknownError'})
    }
}
