import feBlend from './nodeTypes/feBlend'
import feColorMatrix from './nodeTypes/feColorMatrix'
import feComponentTransfer from './nodeTypes/feComponentTransfer'
import feComponentTransferChannel from './nodeTypes/feComponentTransferChannel'
import feComposite from './nodeTypes/feComposite'
import feConvolveMatrix from './nodeTypes/feConvolveMatrix'
import feDiffuseLighting from './nodeTypes/feDiffuseLighting'
import feDistantLight from './nodeTypes/feDistantLight'
import fePointLight from './nodeTypes/fePointLight'
import feSpotLight from './nodeTypes/feSpotLight'
import feDisplacementMap from './nodeTypes/feDisplacementMap'
import feFlood from './nodeTypes/feFlood'
import feGaussianBlur from './nodeTypes/feGaussianBlur'
import feImage from './nodeTypes/feImage'
import feMerge from './nodeTypes/feMerge'
import feMorphology from './nodeTypes/feMorphology'
import feOffset from './nodeTypes/feOffset'
import feSpecularLighting from './nodeTypes/feSpecularLighting'
import feTile from './nodeTypes/feTile'
import feTurbulence from './nodeTypes/feTurbulence'

export default {
    feBlend,
    feColorMatrix,
    feComponentTransfer,
    feComponentTransferChannel,
    feComposite,
    feConvolveMatrix,
    feDiffuseLighting,
    feDistantLight,
    fePointLight,
    feSpotLight,
    feDisplacementMap,
    feFlood,
    feGaussianBlur,
    feImage,
    feMerge,
    feMorphology,
    feOffset,
    feSpecularLighting,
    feTile,
    feTurbulence,
}
