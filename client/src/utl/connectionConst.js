const vertexRoles = {
    input: 'input',
    output: 'output',
}

const connectionTypes = {
    vertex: 'vertex',
    cursor: 'cursor',
}

const vertexTypes = {
    filterEffect: 'filterEffect',
    transferChannel: 'transferChannel',
    lightSource: 'lightSource',
}

export default {
    vertexRoles,
    connectionTypes,
    vertexTypes,
}
