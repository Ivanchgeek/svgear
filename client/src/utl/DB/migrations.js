class MigrationChainError extends Error {
    constructor (message) {
        super(message)
        this.name = 'MigrationChainError'
    }
}

const migrations = [
    {
        from: 0,
        to: 1,
        migrate(db) {
            db.createObjectStore('projects', {keyPath: 'id'})
        }
    }
]

export default function migrate(db, from, to) {
    console.log('migrating', from, to)

    let migrated = from
    const chain = []

    while (migrated !== to) {
        const target = migrations
        .filter(migration => migration.from === migrated)
        .sort((a, b) => {
            if (a.to > b.to) return -1
            if (a.to < b.to) return 1
            return 0
        })[0]

        if (!target) throw new MigrationChainError('Failed to migrate.')
        chain.push(target)
        migrated = target.to
    }

    chain.forEach(migration => migration.migrate(db))
}
