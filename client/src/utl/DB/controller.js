import {v4 as uuid} from 'uuid'
import migrate from "./migrations"
import nodeTypes from "@/utl/nodeTypes"

const DBName = 'svgear'
const activeVersion = 1
const projectsStore = 'projects'

export const projectNotFoundErr = new Error('No such project.')
export const nameConflictError = new Error('Project with such name is already exists.')

class DBController {
    constructor(db) {
        this.db = db
    }

    async createProject(name) {
        const storedProjects = await this.getProjects()

        const transaction = this.db.transaction(projectsStore, 'readwrite')
        const projects = transaction.objectStore(projectsStore)
        
        const data = {
            id: uuid(), 
            name,
            lastChanged: Date.now(),
            nodes: [],
            connections: [],
            vertexes: new Map(),
        }

        if (
            storedProjects.some(project => project.name === data.name && project.id !== data.id)
        ) throw nameConflictError

        const request = projects.put(data)

        return new Promise((resolve, reject) => {
            request.onsuccess = () => resolve(data)
            request.onerror = err => reject(err)
        })
    }

    loadProjectWeak(id, controls) {
        const transaction = this.db.transaction(projectsStore, 'readonly')
        const projects = transaction.objectStore(projectsStore)
        const requset = projects.get(id)
        
        return new Promise((resolve, reject) => {
            requset.onsuccess = function () {
                if (!requset.result) {
                    reject(projectNotFoundErr)
                    return
                }
                const {name, lastChanged} = requset.result

                controls.project.name = name
                controls.project.lastChanged = lastChanged

                resolve()
            }
            requset.onerror = err => reject(err)
        })
    }

    loadProject(id, controls) {
        const transaction = this.db.transaction(projectsStore, 'readonly')
        const projects = transaction.objectStore(projectsStore)
        const requset = projects.get(id)
        
        return new Promise((resolve, reject) => {
            requset.onsuccess = function () {
                if (!requset.result) {
                    reject(projectNotFoundErr)
                    return
                }
                const {nodes, vertexes, connections, name, target, lastChanged} = requset.result

                controls.nodesStore.nodes = nodes.map(node => {
                    const restored = new nodeTypes[node.component](node.name, {
                        x: node.x,
                        y: node.y
                    })
                    delete(node.component)
                    Object.assign(restored, node)
                    return restored
                })
                controls.nodeConnectionsStore.connections = connections
                controls.nodeConnectionsStore.vertexes = vertexes
                controls.project.name = name
                controls.project.lastChanged = lastChanged
                controls.targetStore.applySettings(target)

                resolve()
            }
            requset.onerror = err => reject(err)
        })
    }

    saveProject(id, controls) {
        const transaction = this.db.transaction(projectsStore, 'readwrite')
        const projects = transaction.objectStore(projectsStore)
        
        const nodes = controls.nodesStore.nodes
        .map(node => {return {...node, component: node.component.__name}})
        
        const vertexes = controls.nodeConnectionsStore.vertexes
        const connections = controls.nodeConnectionsStore.connections

        const target = controls.targetStore.target

        const lastChanged = Date.now()
        controls.project.lastChanged = lastChanged

        const data = structuredClone({
            id, nodes, vertexes, connections, target, lastChanged, 
            name: controls.project.name,
        })

        const request = projects.put(data)

        return new Promise((resolve, reject) => {
            request.onsuccess = () => resolve()
            request.onerror = err => reject(err)
        })
    }

    getProjects() {
        const transaction = this.db.transaction(projectsStore, 'readwrite')
        const projects = transaction.objectStore(projectsStore)
        const request = projects.getAll()
        return new Promise((resolve, reject) => {
            request.onsuccess = () => {
                const result = request.result
                if (!result) resolve([])
                resolve(request.result.map(({id, name, lastChanged}) => {return {id, name, lastChanged}}))
            }
            request.onerror = err => reject(err)
        })
    }

    async renameProject(id, newName) {
        const storedProjects = await this.getProjects()
        if (
            storedProjects.some(project => project.name === newName && project.id !== id)
        ) throw nameConflictError

        const transaction = this.db.transaction(projectsStore, 'readwrite')
        const projects = transaction.objectStore(projectsStore)
        const requset = projects.get(id)
        
        return new Promise((resolve, reject) => {
            requset.onsuccess = function () {
                if (!requset.result) {
                    reject(projectNotFoundErr)
                    return
                }
                const project = requset.result
                const changeRequest = projects.put({
                    ...project,
                    name: newName,
                })

                changeRequest.onsuccess = resolve
                changeRequest.onerror = reject
            }
            requset.onerror = err => reject(err)
        })
    }

    async cloneProject(id) {
        const storedProjects = await this.getProjects()

        const transaction = this.db.transaction(projectsStore, 'readwrite')
        const projects = transaction.objectStore(projectsStore)
        const requset = projects.get(id)
        
        return new Promise((resolve, reject) => {
            requset.onsuccess = function () {
                if (!requset.result) {
                    reject(projectNotFoundErr)
                    return
                }
                const project = requset.result
                let cloneName = project.name + ' (clone)'
                while (storedProjects.some(stored => stored.name === cloneName)) cloneName += ' (clone)'

                project.name = cloneName
                project.id = uuid()
                project.lastChanged = Date.now()

                const cloneRequest = projects.add(project)

                cloneRequest.onsuccess = resolve
                cloneRequest.onerror = reject
            }
            requset.onerror = err => reject(err)
        })
    }

    removeProject(id) {
        const transaction = this.db.transaction(projectsStore, 'readwrite')
        const projects = transaction.objectStore(projectsStore)
        const request = projects.delete(id)

        return new Promise((resolve, reject) => {
            request.onsuccess = () => resolve()
            request.onerror = err => reject(err)
        })
    }
}

export default function connectDB() {
    const openRequest = indexedDB.open(DBName, activeVersion)

    return new Promise((resolve, reject) => {
        openRequest.onupgradeneeded = function({oldVersion, newVersion}) {
            migrate(openRequest.result, oldVersion, newVersion)
        }
    
        openRequest.onsuccess = function() {
            resolve(new DBController(openRequest.result))
        }

        openRequest.onerror = function(err) {
            console.err(err)
            reject(err)
        }
    })
}
