import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feOffset.vue'

export default class feOffset extends feNode {
    static nameGenerator = getNameGeneratorFor('Offset')

    constructor() {
        super(component, inputFormats.single, ...arguments)

        this.initValues({
            dx: 0,
            dy: 0,
        })
    }

    render(props) {
        return create('feOffset',
            {
                ...this.rootAttrs(props),
                dx: this.values.dx,
                dy: this.values.dy,
            }
        )
    }
}
