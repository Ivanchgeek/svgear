import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feDiffuseLighting.vue'

export default class feDiffuseLighting extends feNode {
    static nameGenerator = getNameGeneratorFor('Diffuse Lighting')

    constructor() {
        super(component, inputFormats.single, ...arguments)

        this.initValues({
            surfaceScale: 1,
            diffuseConstant: 1,
            lightingColor: '#ffffff',

            useSeparetedKernelUnitLength: false,
            kernelUnitLength: 1,
            kernelUnitLengthX: 1,
            kernelUnitLengthY: 1,
        })
    }

    render(props) {
        const kernelUnitLength = this.values.useSeparetedKernelUnitLength ?
            `${this.values.kernelUnitLengthX} ${this.values.kernelUnitLengthY}`:
            this.values.kernelUnitLength            

        return create('feDiffuseLighting', 
            {
                ...this.rootAttrs(props),
                surfaceScale: this.values.surfaceScale,
                diffuseConstant: this.values.diffuseConstant,
                lightingColor: this.values.lightingColor,
                kernelUnitLength,
            }, 
            [
                create(props.source)
            ]
        )
    }
}
