import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feGaussianBlur.vue'

export default class feGaussianBlur extends feNode {
    static nameGenerator = getNameGeneratorFor('Gaussian Blur')

    constructor() {
        super(component, inputFormats.single, ...arguments)

        this.initValues({
            stdDeviation: 2,
            edgeMode: 'none',
        })
    }

    render(props) {
        const advancedOptions = {}
        if (this.values.edgeMode !== 'none') advancedOptions.edgeMode = this.values.edgeMode

        return create('feGaussianBlur',
            {
                ...this.rootAttrs(props),
                stdDeviation: this.values.stdDeviation,
                ...advancedOptions,
            }
        )
    }
}
