import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feTurbulence.vue'

export default class feTurbulence extends feNode {
    static nameGenerator = getNameGeneratorFor('Turbulence')

    constructor() {
        super(component, inputFormats.none, ...arguments)

        this.initValues({
            baseFrequency: 0.05,
            useSeparatedBaseFrequency: false,
            baseFrequencyX: 0.05,
            baseFrequencyY: 0.05,

            numOctaves: 1,
            seed: 0,
            stitchTiles: true,
            type: 'turbulence',
        })
    }

    render(props, children) {
        const baseFrequency = this.values.useSeparatedBaseFrequency ? 
            `${this.values.baseFrequencyX} ${this.values.baseFrequencyY}` :
            this.values.baseFrequency

        const stitchTiles = this.values.stitchTiles ? 'stitch' : 'noStitch'

        const { numOctaves, seed, type } = this.values

        return create('feTurbulence',
            {
                ...this.rootAttrs(props),
                baseFrequency,
                numOctaves,
                seed,
                type,
                stitchTiles,
            },
            children
        )
    }
}
