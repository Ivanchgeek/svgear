import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feDistantLight.vue'

export default class feDistantLight extends feNode {
    static nameGenerator = getNameGeneratorFor('Distant Light')

    constructor() {
        super(component, inputFormats.none, ...arguments)
        
        this.initValues({
            azimuth: 180,
            elevation: 60,
        })

        this.markSlave()
    }

    render(props) {
        return create('feDistantLight',
            {
                ...this.rootAttrs(props),
                ...this.values,
            }
        )
    }
}
