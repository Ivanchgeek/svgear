import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feTile.vue'

export default class feTile extends feNode {
    static nameGenerator = getNameGeneratorFor('Tile')

    constructor() {
        super(component, inputFormats.single, ...arguments)
    }

    render(props) {
        return create('feTile', this.rootAttrs(props))
    }
}
