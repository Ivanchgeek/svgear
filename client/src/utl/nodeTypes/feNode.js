import { markRaw } from 'vue'
import {v4 as uuid} from 'uuid'
import inputFormats from './inputFormats'

export default class feNode {
    values = {}
    slave = false
    root = false
    inputFormat = inputFormats.none

    constructor(component, inputFormat, namePack, position) {
        this.id = uuid()
        this.component = markRaw(component)
        this.name = namePack.name
        this.FAQName = namePack.FAQName
        this.x = position.x
        this.y = position.y
        this.inputFormat = inputFormat

        this.values = {
            defineArea: false,
            area: {
                x: '0',
                y: '0',
                width: '100%',
                height: '100%',
            }
        }

        switch (inputFormat) {
            case inputFormats.single:
                this.values.in1 = 'SourceGraphic'
                break
            case inputFormats.double:
                this.values.in1 = 'SourceGraphic'
                this.values.in2 = 'SourceGraphic'
                break
        }
    }

    initValues(init) {
        this.values = Object.assign(this.values, init)
    }

    markSlave() {
        this.slave = true
        delete this.values.defineArea
        delete this.values.area
    }

    resultKey() {
        return this.name.replaceAll(' ', '_')
    }

    FAQUri() {
        return `/faq/filterElements#` + this.FAQName
    }

    rootAttrs(props) {
        const area = this.values.defineArea ? this.values.area : {}

        const getInput = input => props[input]?.resultKey() || this.values[input]

        let inputs = {}
        if (this.inputFormat >= inputFormats.single) inputs.in = getInput('in1') 
        if (this.inputFormat === inputFormats.double) inputs.in2 = getInput('in2')

        const attrs = {
            ...area,
            ...inputs,
        }

        if (!this.slave) attrs.result = this.resultKey()
        
        return attrs
    }
}
