import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feMorphology.vue'

export default class feMorphology extends feNode {
    static nameGenerator = getNameGeneratorFor('Morphology')

    constructor() {
        super(component, inputFormats.single, ...arguments)

        this.initValues({
            operator: 'erode',
            useSeparatedRadius: false,
            radius: 1,
            radiusX: 1,
            radiusY: 1,
        })
    }

    render(props) {
        const radius = this.values.useSeparatedRadius ?
            `${this.values.radiusX} ${this.values.radiusY}` :
            this.values.radius

        return create('feMorphology',
            {
                ...this.rootAttrs(props),
                operator: this.values.operator,
                radius,
            }
        )
    }
}
