import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feConvolveMatrix.vue'

export default class feConvolveMatrix extends feNode {
    static nameGenerator = getNameGeneratorFor('Convolve Matrix')

    constructor() {
        super(component, inputFormats.single, ...arguments)

        const initialMatrix = new Array(3)
        for (let i = 0; i < 3; i++) initialMatrix[i] = new Array(3).fill(0)
        
        this.initValues({
            kernelMatrix: initialMatrix,
            order: 3,

            edgeMode: 'none',
            
            useSeparatedOrder: false,
            orderX: 3,
            orderY: 3,

            useDivisor: false,
            divisor: 1,

            useBias: false,
            bias: 0,

            useOffset: false,
            targetX: 0,
            targetY: 0,

            useKernelUnitLength: false,
            kernelUnitX: 1,
            kernelUnitY: 1,

            preserveAlpha: false,
        })
    }

    render(props) {
        const order = this.values.useSeparatedOrder ? 
            `${this.values.orderX} ${this.values.orderY}` :
            this.values.order
        
        const advancedOptions = {}
        if (this.values.useDivisor) advancedOptions.divisor = this.values.divisor
        if (this.values.useBias) advancedOptions.bias = this.values.bias
        if (this.values.useOffset) Object.assign(advancedOptions, {targetX: this.values.targetX, targetY: this.values.targetY})
        if (this.values.useKernelUnitLength) advancedOptions.kernelUnitLength = `${this.values.kernelUnitX} ${this.values.kernelUnitY}`
        if (this.values.edgeMode !== 'none') advancedOptions.edgeMode = this.values.edgeMode
        if (this.values.preserveAlpha) advancedOptions.preserveAlpha = this.values.preserveAlpha

        return create('feConvolveMatrix',
            {
                ...this.rootAttrs(props),
                order,
                kernelMatrix: this.values.kernelMatrix.flat().join(' '),
                ...advancedOptions,

            }
        )
    }
}
