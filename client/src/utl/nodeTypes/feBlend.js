import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import {create} from '@/utl/render'
import {default as component} from '@/components/nodes/feBlend.vue'

export default class feBlend extends feNode {
    static nameGenerator = getNameGeneratorFor('Blend')

    constructor() {
        super(component, inputFormats.double, ...arguments)

        this.initValues({
            mode: 'normal'
        })
    }

    render(props, children) {
        return create('feBlend', 
            {
                ...this.rootAttrs(props),
                mode: this.values.mode,
            },
            children
        )
    }
}
