import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feSpotLight.vue'

export default class feSpotLight extends feNode {
    static nameGenerator = getNameGeneratorFor('Spot Light')

    constructor() {
        super(component, inputFormats.none, ...arguments)
        
        this.initValues({
            x: 10,
            y: 10,
            z: 10,
            pointAtX: 0,
            pointAtY: 0,
            pointAtZ: 0,
            specularExponent: 1,
            limitingConeAngle: 90,
        })

        this.markSlave()
    }

    render(props) {
        return create('feSpotLight',
            {
                ...this.rootAttrs(props),
                ...this.values,
            }
        )
    }
}
