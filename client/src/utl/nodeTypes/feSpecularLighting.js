import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feSpecularLighting.vue'

export default class feSpecularLighting extends feNode {
    static nameGenerator = getNameGeneratorFor('Specular Lighting')

    constructor() {
        super(component, inputFormats.single, ...arguments)

        this.initValues({
            surfaceScale: 1,
            specularConstant: 1,
            specularExponent: 1,

            useSeparetedKernelUnitLength: false,
            kernelUnitLength: 1,
            kernelUnitLengthX: 1,
            kernelUnitLengthY: 1,
        })
    }

    render(props) {
        const kernelUnitLength = this.values.useSeparetedKernelUnitLength ?
            `${this.values.kernelUnitLengthX} ${this.values.kernelUnitLengthY}`:
            this.values.kernelUnitLength            

        return create('feSpecularLighting', 
            {
                ...this.rootAttrs(props),
                surfaceScale: this.values.surfaceScale,
                specularConstant: this.values.specularConstant,
                specularExponent: this.values.specularExponent,
                kernelUnitLength,
            }, 
            [
                create(props.source)
            ]
        )
    }
}
