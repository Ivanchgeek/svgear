function getNameTemplate(template) {
    return function(index) {
        return template + (index > 1 ? ` #${index}` : '')
    }
}

export function getNameGeneratorFor(baseName) {

    return function(validator) {
        const nameTemplate = getNameTemplate(baseName)
        let i = 1
        let name = nameTemplate(i)

        while (!validator(name)) name = nameTemplate(++i)

        return {
            name,
            FAQName: 'fe' + baseName.replaceAll(' ', '')
        }
    }
}
