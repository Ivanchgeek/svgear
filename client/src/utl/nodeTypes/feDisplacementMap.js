import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feDisplacementMap.vue'

export default class feDisplacementMap extends feNode {
    static nameGenerator = getNameGeneratorFor('Displacement Map')

    constructor() {
        super(component, inputFormats.double, ...arguments)

        this.initValues({
            scale: 1,
            xChannelSelector: 'A',
            yChannelSelector: 'A',
        })
    }

    render(props) {
        return create('feDisplacementMap',
            {
                ...this.rootAttrs(props),
                scale: this.values.scale,
                xChannelSelector: this.values.xChannelSelector,
                yChannelSelector: this.values.yChannelSelector,
            }
        )
    }
}
