import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feComponentTransfer.vue'

export default class feComponentTransfer extends feNode {
    static nameGenerator = getNameGeneratorFor('Component Transfer')

    constructor() {
        super(component, inputFormats.single, ...arguments)
    }

    render(props) {
        return create('feComponentTransfer',
            {
                ...this.rootAttrs(props)
            },
            [
                create(props.red, {channel: 'R'}),
                create(props.green, {channel: 'G'}),
                create(props.blue, {channel: 'B'}),
                create(props.alpha, {channel: 'A'}),
            ]
        )
    }
}
