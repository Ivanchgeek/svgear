import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feComposite.vue'

export default class feComposite extends feNode {
    static nameGenerator = getNameGeneratorFor('Composite')

    constructor() {
        super(component, inputFormats.double, ...arguments)

        this.initValues({
            operator: 'over',
            k1: 1,
            k2: 1,
            k3: 1,
            k4: 1,
        })
    }

    render(props) {
        const operator = this.values.operator
        const arithmeticVals = operator === 'arithmetic' ? {
            k1: this.values.k1,
            k2: this.values.k2,
            k3: this.values.k3,
            k4: this.values.k4,
        } : {}

        return create('feComposite', 
            {
                ...this.rootAttrs(props),
                operator,
                ...arithmeticVals,
            }
        )
    }
}
