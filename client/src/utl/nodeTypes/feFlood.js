import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feFlood.vue'

export default class feFlood extends feNode {
    static nameGenerator = getNameGeneratorFor('Flood')

    constructor() {
        super(component, inputFormats.none, ...arguments)

        this.initValues({
            floodColor: '#ffffff',
            floodOpacity: 1,
        })
    }

    render(props) {
        return create('feFlood',
            {
                ...this.rootAttrs(props),
                'flood-color': this.values.floodColor,
                'flood-opacity': this.values.floodOpacity,
            }
        )
    }
}
