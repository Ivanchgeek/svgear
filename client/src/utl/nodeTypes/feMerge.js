import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feMerge.vue'

export default class feMerge extends feNode {
    static nameGenerator = getNameGeneratorFor('Merge')

    constructor() {
        super(component, inputFormats.none, ...arguments)

        this.initValues({
            layers: ['#1'],
        })
    }

    render(props) {
        return create('feMerge', this.rootAttrs(props),
            this.values.layers
            .filter(layer => !!props[layer])
            .map(layer => props[layer].resultKey())
            .map(layer => create('feMergeNode', {in: layer}))
        )
    }
}
