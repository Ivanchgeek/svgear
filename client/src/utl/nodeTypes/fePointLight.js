import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/fePointLight.vue'

export default class fePointLight extends feNode {
    static nameGenerator = getNameGeneratorFor('Point Light')

    constructor() {
        super(component, inputFormats.none, ...arguments)
        
        this.initValues({
            x: 0,
            y: 0,
            z: 0,
        })

        this.markSlave()
    }

    render(props) {
        return create('fePointLight',
            {
                ...this.rootAttrs(props),
                ...this.values,
            }
        )
    }
}
