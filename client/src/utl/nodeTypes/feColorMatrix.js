import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feColorMatrix.vue'

export default class feColorMatrix extends feNode {
    static nameGenerator = getNameGeneratorFor('Color Matrix')

    constructor() {
        super(component, inputFormats.single, ...arguments)

        const initialMatrix = new Array(4)
        for (let i = 0; i < 4; i++) initialMatrix[i] = new Array(5).fill(0)
        
        this.initValues({
            type: 'matrix',
            matrix: initialMatrix,
            saturate: 0.3,
            rotate: 90,
        })
    }

    render(props, children) {
        const {type, matrix, saturate, rotate } = this.values
        const attrs = {type}

        switch (type) {
            case 'matrix':
                attrs.values = matrix.flat().join(' ')
                break
            case 'saturate':
                attrs.values = saturate
                break
            case 'hueRotate':
                attrs.values = rotate
                break
            case 'luminanceToAlpha':
                break
        }


        return create('feColorMatrix', 
            {
                ...this.rootAttrs(props),
                ...attrs,
            },
            children
        )
    }
}
