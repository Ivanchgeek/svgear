import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feComponentTransferChannel.vue'

export default class feComponentTransferChannel extends feNode {
    static nameGenerator = getNameGeneratorFor('Transfer Channel')

    constructor() {
        super(component, inputFormats.none, ...arguments)
        
        this.initValues({
            type: 'identity',
            table: Array(6).fill(0),
            discrete: Array(6).fill(0),
            amplitude: 0.5,
            exponent: 0.5,
            offset: 0.5,
            slope: 0.5,
            intercept: 0.5,
        })

        this.markSlave()
    }

    render(props) {
        const {type, table, discrete, slope, intercept, amplitude, exponent, offset} = this.values
        const attrs = {type}

        switch (type) {
            case 'identity':
                break
            case 'table':
                attrs.tableValues = table.join(' ')
                break
            case 'discrete':
                attrs.tableValues = discrete.join(' ')
                break
            case 'linear':
                Object.assign(attrs, {
                    slope, intercept
                })
                break
            case 'gamma':
                Object.assign(attrs, {
                    amplitude, exponent, offset
                })
                break
        }

        return create('feFunc' + props.channel,
            {
                ...this.rootAttrs(props),
                ...attrs,
            },
        )
    }
}
