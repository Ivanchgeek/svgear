import feNode from './feNode'
import { getNameGeneratorFor } from './nameUtils'
import inputFormats from './inputFormats'
import { create } from '@/utl/render'
import {default as component} from '@/components/nodes/feImage.vue'

export default class feImage extends feNode {
    static nameGenerator = getNameGeneratorFor('Image')

    constructor() {
        super(component, inputFormats.none, ...arguments)

        this.initValues({
            href: '',
            preserveAspectRatio: 'xMidYMid meet',
            crossorigin: 'anonymous',
        })
    }

    render(props) {
        return create('feImage',
            {
                ...this.rootAttrs(props),
                preserveAspectRatio: this.values.preserveAspectRatio,
                crossorigin: this.values.crossorigin,
                href: this.values.href,
            }
        )
    }
}
