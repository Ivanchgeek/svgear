import connectionConst from './connectionConst'

export function create(element, attributes = {}, children = []) {
    if (element === undefined) return

    if (element.render) {
        return element.render(attributes, children)
    }

    const dom = document.createElementNS('http://www.w3.org/2000/svg', element)
    
    for (let attr in attributes) {
        dom.setAttribute(attr, attributes[attr])
    }

    for (let child of children) {
        if (!child) continue
        dom.appendChild(child)
    }

    return dom
}

export function traceConnectionsFor(id, nodes, connections) {
    const tracked = {}

    for (let {from, to} of connections) {
        if (
            to.type !== connectionConst.connectionTypes.vertex ||
            to.nodeId !== id ||
            from.type !== connectionConst.connectionTypes.vertex
        ) continue

        const target = nodes.find(({id}) => id === to.nodeId)
        const src = nodes.find(({id}) => id === from.nodeId)
        if (!target || !src) continue

        tracked[to.vertexName] = src
    }

    return tracked
}

export function getRenderQueue(nodes, connections) {
    const renderableNodes = nodes.filter(({slave}) => !slave)
    const renderableNodesIds = renderableNodes.map(({id}) => id)
    const rootId = renderableNodes.find(({root}) => root)?.id

    if (!rootId) throw Error('Root node is required.')

    const clearConnections = connections
        .map(connection => {
            return {from: connection.from.nodeId, to: connection.to.nodeId}
        })
        .filter(({from, to}) => renderableNodesIds.includes(from) && renderableNodesIds.includes(to))

    const activeDetectionGraph = new Map()
    clearConnections.forEach(({from, to}) => {
        if (!activeDetectionGraph.has(to)) activeDetectionGraph.set(to, new Set())
        activeDetectionGraph.get(to).add(from)
    })

    const activeNodes = new Set([rootId])
    const currentPath = [rootId]
    while (currentPath.length !== 0) {
        const base = currentPath.at(-1)
        const options = Array.from(activeDetectionGraph.get(base) || [])
        const choice = options.find(option => !activeNodes.has(option))

        if (choice) {
            currentPath.push(choice)
            activeNodes.add(choice)
        } else {
            currentPath.pop()
        }
    }

    const activeConnections = clearConnections.filter(({to}) => activeNodes.has(to))

    const childrenGraph = new Map()
    activeConnections.forEach(({from, to}) => {
        if (!childrenGraph.has(from)) childrenGraph.set(from, new Set())
        if (to !== rootId) childrenGraph.get(from).add(to)
    })

    const renderQueue = [rootId]
    while (childrenGraph.size !== 0) {
        const next = Array.from(childrenGraph.keys())
            .find(node => childrenGraph.get(node).size === 0)
        if (!next) throw Error('Nodes loop detected.')

        renderQueue.unshift(next)
        childrenGraph.delete(next)
        Array.from(childrenGraph.values()).forEach(children => children.delete(next))
    }

    return renderQueue.map(item => renderableNodes.find(({id}) => item === id))
}

export function getFilter(nodes, connections, options) {
    return(
        create('svg', 
            {
                x: 0,
                y: 0,
                width: 0,
                height: 0,
                style: 'width: 0; height: 0; position: fixed; top: 0; left: 0;',
            }, 
            [
                create('filter',
                    {
                        x: 0,
                        y: 0,
                        width: '100%',
                        height: '100%',
                        ...options
                    },
                    getRenderQueue(nodes, connections)
                        .map(node => 
                            create(node, traceConnectionsFor(node.id, nodes, connections))
                        )
                )
            ]
        )
    ) 
}

export function filterToString(filter) {
    const xsltDoc = new DOMParser().parseFromString(`
        <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
          <xsl:template match="node()|@*">
            <xsl:copy><xsl:apply-templates select="node()|@*"/></xsl:copy>
          </xsl:template>
          <xsl:output indent="yes" method="xml"/>
          <xsl:strip-space  elements="node()|@*"/>
        </xsl:stylesheet>
    `, 'application/xml')

    const xsltProcessor = new XSLTProcessor()    
    xsltProcessor.importStylesheet(xsltDoc)
    const resultDoc = xsltProcessor.transformToDocument(filter)
    const resultXml = new XMLSerializer().serializeToString(resultDoc)
    return resultXml
}
