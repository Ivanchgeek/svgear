import { createRouter, createWebHistory } from 'vue-router'

import IDE from '@/views/IDE'
import ErrorView from '@/views/ErrorView'
import ProjectsList from '@/views/ProjectsList'
import EditProject from '@/views/EditProject'
import FAQ from '@/views/FAQ'

import NotFoundImg from '@/assets/404.png'
import UnknownError from '@/assets/unknown-error.png'

const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/projects'
  },
  {
    path: '/projects',
    name: 'Projects',
    component: ProjectsList,
    meta: {
      tentacle: {
        topRight: 4,
        bottomLeft: 6,
      }
    }
  },
  {
    path: '/projects/:id',
    name: 'Project',
    component: EditProject,
    meta: {
      tentacle: {
        topLeft: 5,
        bottomRight: 7,
      }
    }
  },
  {
    path: '/projects/:id/ide',
    name: 'IDE',
    component: IDE,
  },
  {
    path: '/faq/:topic?',
    name: 'FAQ',
    component: FAQ,
  },
  {
    path: '/error',
    name: 'UnknownError',
    component: ErrorView,
    meta: {
      img: UnknownError,
      text: 'errorPages.unknown'
    }
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: ErrorView,
    meta: {
      img: NotFoundImg,
      text: 'errorPages.notFound'
    }
  }
]

const router = new createRouter({
  history: createWebHistory(),
  routes
})

export default router
