export default {
    home: {
        welcome: `Welcome to svgear!`,
        about1: `It's a powerfull tool to build { svgFiltersDoc }.`,
        svgFilters: `svg filters`,
        about2: `
            The filter development process consists of adding graphical nodes with all the settings for 
            each filter primitive and simply connecting them to each other to 
            reflect the processing order of your filter.
        `,
        about3: `
            Many filter targets are available [raster image, svg, web page]. 
            Resulting filter structure can be exported in one click.
            Learn more in the { FAQ }.
        `,
        noProjects: `You have no projects yet.`,
        lastModified: `last modified`,
        createNewProject: `Create new project`,
    },
    editProject: {
        title: `Edit project`,
        rename: `rename`,
        clone: `clone`,
        delete: `delete`,
        projectDialog: `{ action } project { project }?`
    },
    dialog: {
        confirm: {
            ok: `ok`,
            cancel: `cancel`,
        }
    },
    IDE: {
        editor: {
            explorer: {
                title: `Nodes explorer`,
                empty: `Add some nodes via context menu`,
            },
            scale: `scale`,
        },
        target: {
            errorOccured: `Error eccured`,
            toolsInactive: `Svgear tools aren't active`,
            sctiptIncludeMsg: `Include following script to your page to proceed development`,
            settings: {
                preset: `preset`,
                presets: {
                    web: `web`,
                    image: `image`,
                },
                sourceUrl: `source url`,
                targetSelector: `target selector`,
                image: `image`,
                reset: `reset`,
                position: `position`,
                size: `size`,
                apply: `apply`,
                exit: `exit`,
            }
        },
        notSavedAlert: {
            soft: `Project not saved. Save before close?`,
            hard: `Project not saved. Close page?`,
            save: `save`,
            exit: `exit`,
        },
        context: {
            addNode: `Add Node`,
            deleteNode: `Delete Node`,
            markAsRoot: `Mark As Root`,
            defineArea: `Define Area`,
            disableArea: `Disable Area`,
            openFAQ: `[?] About Node`,
        },
        nodes: {
            output: `output`,
            areaSettings: `Area settings`,
            advancedOptions: `Advanced Options`,
            feImage: {
                reset: `reset`,
            }
        },
        components: {
            codeBlock: {
                copyText: `copy`,
                copiedText: `copied!`,
            },
            imageInput: {
                instructions: `Drop your image here or`,
                browse: `browse it`,
                dropIt: 'Drop it',
            },
        },
    },
    errorPages: {
        unknown: `Something went wrong.`,
        notFound: `Page not found.`
    },
    faq: {
        filterElements: {
            title: 'Filter Elements',
            sections: {
                feBlend: {
                    title: `Blend`,
                    content: `
                        The feBlend SVG filter primitive composes two objects together ruled by a certain blending mode. 
                        This is similar to what is known from image editing software when blending two layers. 
                        The mode is defined by the mode attribute.
                    `
                },
                feColorMatrix: {
                    title: `Color Matrix`,
                    content: `
                        The feColorMatrix SVG filter element changes colors based on a transformation matrix. 
                        Every pixel's color value [R,G,B,A] is matrix multiplied by a 5 by 5 color matrix to create new color [R',G',B',A'].
                    `
                },
                feComponentTransfer: {
                    title: `Component Transfer`,
                    content: {
                        1: `
                            The feComponentTransfer SVG filter primitive performs color-component-wise remapping of data for each pixel. 
                            It allows operations like brightness adjustment, contrast adjustment, color balance or thresholding.
                        `,
                        2: `
                            The calculations are performed on non-premultiplied color values. 
                            The colors are modified by changing each channel (R, G, B, and A) to the result of what the children 
                            feFuncR, feFuncB, feFuncG, and feFuncA (represented with { transferChannel } node) return.
                        `
                    }
                },
                feComponentTransferChannel: {
                    title: `Component Transfer Channel`,
                    content: `
                        Represents feFuncR, feFuncG, feFuncB and feFuncA SVG filter primitives, which defines the 
                        transfer function for color components of the input graphic of its parent { componentTransfer } element.
                    `
                },
                feComposite: {
                    title: `Composite`,
                    content: `
                        The feComposite SVG filter primitive performs the combination of two input images pixel-wise in image space using one of the 
                        Porter-Duff compositing operations: over, in, atop, out, xor, lighter, or arithmetic.
                    `
                },
                feConvolveMatrix: {
                    title: `Convolve Matrix`,
                    content: `
                        The feConvolveMatrix SVG filter primitive applies a matrix convolution filter effect. 
                        A convolution combines pixels in the input image with neighboring pixels to produce a resulting image. 
                        A wide variety of imaging operations can be achieved through convolutions, including blurring, edge detection, sharpening, embossing and beveling.
                    `
                },
                feDiffuseLighting: {
                    title: `Diffuse Lighting`,
                    content: `
                        The feDiffuseLighting SVG filter primitive lights an image using the alpha channel as a bump map. 
                        The resulting image, which is an RGBA opaque image, depends on the light color, light position and surface geometry of the input bump map.
                        Used with light source nodes: { lightSources }.
                    `
                },
                feDisplacementMap: {
                    title: `Displacement Map`,
                    content: `
                        The feDisplacementMap SVG filter primitive uses the pixel values from the image from in2 to spatially displace the image from in.
                        The formula for the transformation looks like this:
                        { displacementFormula }
                        where P(x,y) is the input image, in, and P'(x,y) is the destination. XC(x,y) and YC(x,y) 
                        are the component values of the channel designated by xChannelSelector and yChannelSelector.
                    `
                },
                feDistantLight: {
                    title: `Distant Light`,
                    content: `
                        The feDistantLight filter primitive defines a distant light source that can be used within a 
                        lighting filter primitive: { diffuseLighting } or { specularLighting }.
                    `
                },
                feFlood: {
                    title: `Flood`,
                    content: `
                        The feFlood filter primitive fills the filter subregion with the color and opacity defined by flood-color and flood-opacity.
                    `
                },
                feGaussianBlur: {
                    title: `Gaussian Blur`,
                    content: `
                        The feGaussianBlur filter primitive blurs the input image by the amount specified in stdDeviation.
                    `
                },
                feImage: {
                    title: `Image`,
                    content: `
                        The feImage filter primitive fetches image data from an external source and provides the pixel data as output 
                        (meaning if the external source is an SVG image, it is rasterized).
                    `
                },
                feMerge: {
                    title: `Merge`,
                    content: `
                        The feMerge filter primitive allows filter effects to be applied concurrently instead of sequentially. 
                        This is achieved by other filters storing their output via the result attribute and then accessing it in a feMerge.
                    `
                },
                feMorphology: {
                    title: `Morphology`,
                    content: `
                        The feMorphology filter primitive is used to erode or dilate the input image. 
                        Its usefulness lies especially in fattening or thinning effects.
                    `
                },
                feOffset: {
                    title: `Offset`,
                    content: `
                        The feOffset filter primitive allows to offset the input image. 
                        The input image as a whole is offset by the values specified in the dx and dy attributes.
                    `
                },
                fePointLight: {
                    title: `Point Light`,
                    content: `
                        The fePointLight filter primitive defines a light source which allows to create a point light effect. 
                        It that can be used within a lighting filter primitive: { diffuseLighting } or { specularLighting }.
                    `
                },
                feSpecularLighting: {
                    title: `Specular Lighting`,
                    content: `
                        The feSpecularLighting filter primitive lights a source graphic using the alpha channel as a bump map. 
                        The resulting image is an RGBA image based on the light color. The lighting calculation follows the standard specular component of the Phong lighting model. 
                        The resulting image depends on the light color, light position and surface geometry of the input bump map. The result of the lighting calculation is added. 
                        The filter primitive assumes that the viewer is at infinity in the z direction.
                        Used with light source nodes: { lightSources }.
                    `
                },
                feSpotLight: {
                    title: `Spot Light`,
                    content: `
                        The feSpotLight filter primitive defines a light source that can be used to create a spotlight effect. 
                        It is used within a lighting filter primitive:  { diffuseLighting } or { specularLighting }.
                    `
                },
                feTile: {
                    title: `Tile`,
                    content: `
                        The feTile filter primitive allows to fill a target rectangle with a repeated, tiled pattern of an input image.
                    `
                },
                feTurbulence: {
                    title: `Turbulence`,
                    content: `
                        The feTurbulence filter primitive creates an image using the Perlin turbulence function. 
                        It allows the synthesis of artificial textures like clouds or marble. 
                        The resulting image will fill the entire filter primitive subregion.
                    `
                },
            },
        },
        basics: {
            title: `Basics`,
            sections: {
                aboutTool: {
                    title: `About Tool`,
                    content: {
                        1: `Welcome to svgear! It's a powerfull tool to build { svgFiltersDoc }.`,
                        svgFilters: `svg filters`,
                        2: `
                            The filter development process consists of adding graphical nodes with all the settings for 
                            each filter primitive and simply connecting them to each other to 
                            reflect the processing order of your filter.
                        `,
                        3: `
                            Many filter targets are available [raster image, svg, web page]. 
                            Resulting filter structure can be exported in one click.
                        `,
                    }
                },
                nodes: {
                    title: `Nodes`,
                    content: `
                        The basis for building your filter is the nodes of primitives. 
                        To add a new node, open the context menu in the nodes area (bottom block of the editor) and select the desired sub-item in the "Add Node" group.
                        Each node contains a set of different parameter settings, the change of which entails a change in the effect applied to the node's input.
                    `
                },
                nodesConnections: {
                    title: `Nodes Connections`,
                    content: `
                        Any filter is a hierarchical sequence of filtering primitives. 
                        The output of one primitive becomes the input of the next, as a result, the filter creates a complex, complex graphic effect.
                        To display this relationship, you just need to find the node whose result you want to transform the [{ nodeVertex }] element
                        in front of the "output" block and use the cursor to draw a line from it to the corresponding input element of the second node.
                    `
                },
                rootNode: {
                    title: `Root Node`,
                    content: `
                        The root node is the node whose result will be final in the context of the filter. 
                        It is he who usually combines the results of all other nodes.
                        In the interface, this node is marked with the [{ rootNodeMark }] icon.
                        You can mark a node as a root node through it's context menu.
                    `
                },
                example: {
                    title: `Example`,
                    content: {
                        1: `
                            As an example, consider creating the effect of adding a shadow to an svg element.
                            As a source, we have the following code:
                        `,
                        2: `
                            Which implements this image.
                        `,
                        3: `
                            The shadow will be implemented by blurring the original image 
                            (in fact, a transparency map will be used for blurring, which in this case contains a black silhouette of the element) 
                            with subsequent displacement and superimposition on top of the shadow of the original image. First, let's create a filter that implements the shadow without overlapping. 
                            In the form of nodes, it will be implemented as follows:
                        `,
                        4: `
                            After exporting the filter, you will get the following code:
                        `,
                        5: `
                            Now we will combine the resulting shadow with the original image, add Merge nodes (to combine layers),
                            as well as Offset with zero offset (to get a layer with an unchanged input image).
                            It is important not to forget to make the Merge node the root node.
                        `,
                        6: `
                            Now it remains to export the filter, add it to the image file and apply it to the element. 
                            As a result, the following result will be obtained.
                        `,
                        7: `
                            The final image:
                        `,
                    }
                },
            }
        }
    }
}
