import { createI18n } from 'vue-i18n'
import messages from './messages'

const localStorageKey = 'i18n-locale'

export function saveLocale(locale) {
    localStorage.setItem(localStorageKey, locale)
}

const i18n = createI18n({
    legacy: false,
    locale: window.localStorage.getItem(localStorageKey) || 'en',
    fallbackLocale: 'en',
    messages,
})

export default i18n
