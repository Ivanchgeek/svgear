import en from './messages/en'
import ru from './messages/ru'

export default {
    en,
    ru,
}
