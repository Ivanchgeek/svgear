import { defineStore } from 'pinia'
import { ref } from 'vue'
import { v4 as uuiv4 } from 'uuid'


const useContextMenu = defineStore('context-menu', () => {
    
    const points = ref([])
    let tag = uuiv4()

    function addPoints(data) {
        points.value.push(
            ...data
            .map((point, i) => Object.assign({separate: i == 0}, point))
            .map(point => Object.assign(point, { tag }))
        )
    }

    function clearOldPoints() {
        points.value = points.value.filter(point => point.tag === tag)
        tag = uuiv4()
    }

    function addPoint(point) {
        addPoints([point])
    }

    return {
        points,
        addPoints,
        addPoint,
        clearOldPoints,
    }
    
})

export default useContextMenu
