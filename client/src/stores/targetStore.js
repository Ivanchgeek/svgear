import { defineStore } from 'pinia'
import { reactive, readonly } from 'vue'

const useTargetStore = defineStore('target', () => {

    const initialState = () => {return {
        preset: 'web',
        url: 'http://example.com',
        image: '',
        selector: '#need_filter',
        filterArea: {
            x: '0',
            y: '0',
            width: '100%',
            height: '100%',
        }
    }}
    
    const target = reactive(initialState())

    function applySettings(settings) {
        Object.assign(target, settings)
    }

    const needReload = reactive({
        input: false,
        result: false,
        floatingResult: false,
    })

    function callReload() {
        Object.keys(needReload).forEach(name => needReload[name] = true)
    }

    function markReloaded(name) {
        if (Object.keys(needReload).includes(name)) needReload[name] = false
    }

    function $reset() {
        Object.assign(target, initialState())
    }

    return {
        target: readonly(target),
        applySettings,
        needReload: readonly(needReload),
        callReload,
        markReloaded,
        $reset,
    }
    
})

export default useTargetStore
