import { defineStore } from 'pinia'
import { readonly, reactive, ref } from 'vue'
import useNodesStore from './nodesStore'
import useNodeConnections from './nodeConnectionsStore'
import useNodeEditorOptions from './nodeEditorOptionsStore'
import useTargetStore from './targetStore'
import connectDB from '@/utl/DB/controller'

const useProjectStore = defineStore('project', () => {

    const project = reactive({
        name: '',
        id: '',
        lastChanged: undefined
    })
    const modified = ref(false)
    let blockModifications = false

    const controls = {
        nodesStore: useNodesStore(),
        nodeConnectionsStore: useNodeConnections(),
        nodeEditorOptionsStore: useNodeEditorOptions(),
        targetStore: useTargetStore(),
        project,
    }

    const markModified = () => !blockModifications && (modified.value = true)
    controls.nodesStore.$subscribe(markModified)
    controls.nodeConnectionsStore.$subscribe(markModified)
    controls.targetStore.$subscribe(markModified)

    let connection = connectDB().then(result => {
        return result
    })

    async function getProjects() {
        const controller = await connection
        return controller.getProjects()
    }

    async function create(name) {
        const controller = await connection
        return controller.createProject(name)
    }

    async function load(id) {
        const controller = await connection
        controls.nodesStore.$reset()
        controls.nodeConnectionsStore.$reset()
        controls.targetStore.$reset()
        project.id = id
        modified.value = false
        blockModifications = true
        return controller.loadProject(project.id, controls)
            .then(r => {
                blockModifications = false
                return r
            })
    }

    async function loadWeak(id) {
        const controller = await connection
        project.id = id
        modified.value = false
        blockModifications = true
        return controller.loadProjectWeak(project.id, controls)
            .then(r => {
                blockModifications = false
                return r
            })
    }

    async function save() {
        const controller = await connection
        modified.value = false
        return controller.saveProject(project.id, controls)
    }

    async function rename(newName) {
        const controller = await connection
        return controller.renameProject(project.id, newName)
            .then(r => {
                project.name = newName
                return r
            })
    }

    async function clone() {
        const controller = await connection
        return controller.cloneProject(project.id)
    }

    async function remove() {
        const controller = await connection
        return controller.removeProject(project.id)
    }

    return {
        project: readonly(project),
        modified,
        getProjects,
        load,
        loadWeak,
        create,
        save,
        rename,
        clone,
        remove,
    }
    
})

export default useProjectStore
