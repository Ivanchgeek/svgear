import { defineStore } from 'pinia'
import { ref } from 'vue'

const useLoadingStore = defineStore('loading', () => {
    
    const loading = ref(false)

    return {
        loading
    }
    
})

export default useLoadingStore
