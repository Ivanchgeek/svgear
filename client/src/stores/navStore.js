import { defineStore } from 'pinia'
import { ref, watch } from 'vue'

const useNavStore = defineStore('navbar', () => {
    
    const buttons = ref([])
    const heading = ref('')

    function addButton(key, icon, click, classes = []) {
        buttons.value.push({key, icon, click, classes})
    }

    function deleteButton(key) {
        buttons.value = buttons.value.filter(({key: btnKey}) => btnKey !== key)
    }

    function patchButton(key, patch) {
        const target = buttons.value.find(({key: btnKey}) => btnKey === key)
        if (!target) return
        Object.assign(target, patch)
    }

    watch(heading, () => {
        const subTitle = heading.value
        document.title = 'svgear' + (subTitle !== '' ? ` | ${subTitle}` : '')
    })

    return {
        buttons,
        heading,
        addButton,
        deleteButton,
        patchButton,
    }
    
})

export default useNavStore
