import { defineStore } from 'pinia'
import { reactive } from 'vue'

const useNodeEditorOptions = defineStore('node-editor-options', () => {
    
    const nodesContainerOptions = reactive({
        width: 12e3,
        height: 3e3,
        cursor: {
            x: 0,
            y: 0,
        }
    })

    function updateContainerCursor(x, y) {
        Object.assign(nodesContainerOptions.cursor, {x, y})
    }

    const nodesViewOptions = reactive({
        width: 0,
        height: 0,
        scrollRequest: {
            x: 0,
            y: 0,
        },
        maximized: false,
        scale: 100,
        scaleMin: 70,
        scaleMax: 120,
    })

    function bindNodesViewSize(width, height) {
        Object.assign(nodesViewOptions, {width, height})
    }

    function requestViewScroll(x, y) {
        Object.assign(nodesViewOptions, {scrollRequest: {x, y}})
    }

    function toggleNodesViewMaximized() {
        nodesViewOptions.maximized = !nodesViewOptions.maximized
    }

    const resultViewOptions = reactive({
        maximized: false
    })

    function toggleResultViewMaximized() {
        resultViewOptions.maximized = !resultViewOptions.maximized
    }

    return {
        nodesContainerOptions,
        updateContainerCursor,
        nodesViewOptions,
        bindNodesViewSize,
        requestViewScroll,
        toggleNodesViewMaximized,
        resultViewOptions,
        toggleResultViewMaximized,
    }
})

export default useNodeEditorOptions
