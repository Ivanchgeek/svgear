import { defineStore } from 'pinia'
import { markRaw, readonly, ref } from 'vue'

const useDialogStore = defineStore('dialog', () => {
    
    const visible = ref(false)
    const form = ref(undefined)
    const formOptions = ref({})

    let resolveResult = () => {}
    let rejectResult = () => {}

    function callDialog(component, options = {}) {
        form.value = markRaw(component)
        formOptions.value = options
        visible.value = true
        return new Promise((resolve, reject) => {
            resolveResult = resolve
            rejectResult = reject
        })
    }

    function finishDialogWith(action, result) {
        visible.value = false
        action(result)
    }

    return {
        form: readonly(form),
        formOptions: readonly(formOptions),
        visible,
        callDialog,
        resolve: result => finishDialogWith(resolveResult, result),
        reject: result => finishDialogWith(rejectResult, result),
    }
    
})

export default useDialogStore
