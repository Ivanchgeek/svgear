import { defineStore } from 'pinia'
import { ref } from 'vue'
import connectionConst from '@/utl/connectionConst'

const useNodeConnections = defineStore('node-connections', () => {

    const vertexes = ref(new Map())
    const connections = ref([])

    function registerVertex(nodeId, vertex) {
        if (!vertexes.value.has(nodeId)) vertexes.value.set(nodeId, [])
        if (vertex.type === undefined) vertex.type = connectionConst.vertexTypes.filterEffect
        vertexes.value.set(nodeId, 
                vertexes.value.get(nodeId)
                .filter(registered => registered.name !== vertex.name)
                .concat(vertex)
            )
    }

    function getVertex(nodeId, vertexName) {
        const pull = vertexes.value.get(nodeId)
        if (!pull) return
        return pull.find(vertex => vertex.name === vertexName)
    }

    function initConnectionFrom(nodeId, vertexName) {
        const vertex = getVertex(nodeId, vertexName)
        if (!vertex || vertex.role !== connectionConst.vertexRoles.output) return

        connections.value.push({
            from: {
                type: connectionConst.connectionTypes.vertex,
                nodeId,
                vertexName,
            },
            to: {
                type: connectionConst.connectionTypes.cursor,
            }
        })
    }

    function softizeConnection(nodeId, vertexName) {
        const vertex = getVertex(nodeId, vertexName)
        if (!vertex || vertex.role !== connectionConst.vertexRoles.input) return

        const connection = connections.value.find(({ to }) => 
                to.type === connectionConst.connectionTypes.vertex &&
                to.nodeId === nodeId &&
                to.vertexName === vertexName
            )

        if (!connection) return
        connection.to.soft = true
    }

    function isConnectionCondidate(nodeId, vertexName) {
        const connection = connections.value.find(connection => connection.to.type === connectionConst.connectionTypes.cursor)
        if (!connection) return true

        if (nodeId === connection.from.nodeId) return false

        const targetVertex = getVertex(nodeId, vertexName)
        if (
            !targetVertex ||
            targetVertex.role !== connectionConst.vertexRoles.input ||
            isVertexConnected(nodeId, vertexName)
        ) return false

        const sourceVertex = getVertex(connection.from.nodeId, connection.from.vertexName)
        if (
            !sourceVertex ||
            sourceVertex.type !== targetVertex.type
        ) return false

        return true
    }

    function softFinishConnect(nodeId, vertexName) {
        if (!isConnectionCondidate(nodeId, vertexName)) return

        const connection = connections.value.find(connection => connection.to.type === connectionConst.connectionTypes.cursor)
        if (!connection) return
        connection.to = {
            type: connectionConst.connectionTypes.vertex,
            soft: true,
            nodeId,
            vertexName,
        }
    }

    function dropSoftFinishConnect() {
        const connection = connections.value.find(connection => connection.to.soft)
        if (!connection) return
        connection.to = {
            type: connectionConst.connectionTypes.cursor,
            soft: false,
        }
    }

    function finishConnection() {
        const floatingIndex = connections.value.findIndex(connection => connection.to.type === connectionConst.connectionTypes.cursor)
        if (floatingIndex !== -1) connections.value.splice(floatingIndex, 1)

        const finishedConnection = connections.value.find(connection => connection.to.soft)
        if (finishedConnection) delete finishedConnection.to.soft
    }

    function isVertexConnected(nodeId, vertexName) {
        return connections.value.some(connection => {
            const check = entity => entity.type === connectionConst.connectionTypes.vertex && entity.nodeId === nodeId && entity.vertexName === vertexName
            return check(connection.from) || check(connection.to)
        })
    }

    function deleteVertexes(nodeId) {
        vertexes.value.delete(nodeId)
        connections.value = connections.value.filter(
            connection => !(connection.from.nodeId === nodeId || connection.to.nodeId === nodeId)
        )
    }

    function unregisterVertex(nodeId, vertexName) {
        if (!vertexes.value.has(nodeId)) return
        vertexes.value.set(nodeId, vertexes.value.get(nodeId).filter(
            vertex => vertex.name !== vertexName
        ))
        connections.value = connections.value.filter(
            connection => !(
                connection.from.nodeId === nodeId && connection.from.vertexName === vertexName || 
                connection.to.nodeId === nodeId && connection.to.vertexName === vertexName
            )
        )
    }

    function requestVertexesUpdate(nodeId) {
        if (!vertexes.value.has(nodeId)) return
        vertexes.value.get(nodeId).forEach(vertex => vertex.needUpdate = true)
    }

    function updateVertex(nodeId, vertexName, update) {
        if (!vertexes.value.has(nodeId)) return
        const target = vertexes.value.get(nodeId).findIndex(vertex => vertex.name === vertexName)
        Object.assign(vertexes.value.get(nodeId)[target], update)
        delete vertexes.value.get(nodeId)[target].needUpdate
    }
    
    function $reset() {
        vertexes.value = new Map()
        connections.value = []
    }

    return {
        connections,
        vertexes,
        registerVertex,
        getVertex,
        initConnectionFrom,
        softizeConnection,
        isConnectionCondidate,
        softFinishConnect,
        dropSoftFinishConnect,
        finishConnection,
        isVertexConnected,
        deleteVertexes,
        unregisterVertex,
        requestVertexesUpdate,
        updateVertex,
        $reset,
    }
})

export default useNodeConnections
