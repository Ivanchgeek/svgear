import { defineStore } from 'pinia'
import { computed, reactive, ref } from 'vue'
import useNodeEditorOptions from './nodeEditorOptionsStore'
import useNodeConnections from './nodeConnectionsStore'

const useNodesStore = defineStore('nodes', () => {
    
    const nodeEditorOptions = useNodeEditorOptions()
    const nodeConnections = useNodeConnections()

    const nodes = ref([])

    function createNode(type, position) {
        const name = type.nameGenerator(name => !nodes.value.some(node => node.name === name))
        const node = new type(name, position)
        if (nodes.value.length === 0) node.root = true
        nodes.value.push(node)
    }

    function getNode(id) {
        return nodes.value.find(node => node.id === id)
    }

    function markNodeAsRoot(id) {
        const oldRoot = nodes.value.find(({root}) => root)
        const target = getNode(id)

        if (target) {
            target.root = true
            if (oldRoot) oldRoot.root = false
        }
    }

    function deleteNode(id) {
        nodes.value = nodes.value.filter(node => node.id !== id)
        nodeConnections.deleteVertexes(id)
    }

    function changeNodeName(id, newName) {
        const trimmed = newName.trim()
        if (!trimmed) return
        if (nodes.value.some(node => node.name === trimmed)) return
        getNode(id).name = trimmed
    }

    const grabbedNode = reactive({})

    function grabNode(id) {
        const node = getNode(id)
        Object.assign(grabbedNode, {
            id, 
            offsetX: node.x - nodeEditorOptions.nodesContainerOptions.cursor.x,
            offsetY: node.y - nodeEditorOptions.nodesContainerOptions.cursor.y
        })
    }

    const nodeGrabbed = computed(() => !!grabbedNode.id)

    function moveNodeTo(id, x, y) {
        Object.assign(getNode(id), {x, y})
    }

    function dropNode() {
        const target = getNode(grabbedNode.id)
        if (!target) return
        nodes.value = nodes.value.filter(node => node.id !== target.id)
        nodes.value.push(target)
        grabbedNode.id = undefined
    }

    function $reset() {
        nodes.value = []
    }

    return {
        nodes,
        createNode,
        getNode,
        markNodeAsRoot,
        deleteNode,
        changeNodeName,
        grabbedNode,
        grabNode,
        nodeGrabbed,
        moveNodeTo,
        dropNode,
        $reset,
    }
})

export default useNodesStore
