import { toValue } from "vue"

export function useCoordinates() {

    function getDocumentCoordinates(ref) {
        const val = toValue(ref)
        if (!val) return {}

        const {left, top} = val.getBoundingClientRect()
        return {
            x: left, 
            y: top,
        }
    }

    function getDocumentCenterCoordinates(ref) {
        const val = toValue(ref)
        if (!val) return {}

        const {top, bottom, left, right} = val.getBoundingClientRect()
        return {
            x: (left + right) / 2,
            y: (top + bottom) / 2,
        }
    }

    return {
        getDocumentCoordinates, 
        getDocumentCenterCoordinates
    }

}
