import {ref, watch} from 'vue'
import * as render from '@/utl/render'
import useTargetStore from '@/stores/targetStore'
import useNodesStore from '@/stores/nodesStore'
import useNodeConnections from '@/stores/nodeConnectionsStore'
import useProjectStore from '@/stores/projectStore'

export function useFilterRender() {
    const targetStore = useTargetStore()
    const nodesStore = useNodesStore()
    const nodeConnectionsStore = useNodeConnections()
    const projectStore = useProjectStore()

    const renderError = ref('')
    const filter = ref(undefined)

    watch([
        () => nodesStore.nodes,
        () => nodeConnectionsStore.connections,
        () => targetStore.target.filterArea,
    ], () => {
        renderError.value = ''
    
        try {
            filter.value = render.filterToString(
                    render.getFilter(
                        nodesStore.nodes, 
                        nodeConnectionsStore.connections, 
                        {
                            id: projectStore.project.name.trim().replaceAll(' ', '-'),
                            x: targetStore.target.filterArea.x,
                            y: targetStore.target.filterArea.y,
                            width: targetStore.target.filterArea.width,
                            height: targetStore.target.filterArea.height,
                        }
                    )
                )
        } catch(e) {
            renderError.value = e.message
            filter.value = undefined
        }
    }, {immediate: true, deep: true})

    return {
        filter,
        renderError,
    }
}