import { useRoute, useRouter } from 'vue-router'
import { computed, inject, provide, ref } from 'vue'

export function useFAQTools() {
    const topicContextKey = 'topic-context'
    const localTopicContext = ref(undefined)

    const route = useRoute()
    const router = useRouter()

    const activeTopic = computed({
        get() {
            return route.params.topic
        },
        set(value) {
            route.params.topic = value
        }
    })

    const activeSection = computed({
        get() {
            return route.hash
        },
        set(value) {
            router.replace({hash: '#' + value})
        }
    })

    function defineTopicContext(topic) {
        const context = section => `faq.${topic}.sections.${section}`
        
        provide(topicContextKey, context)
        localTopicContext.value = context
    }

    const topicContext = computed(() => localTopicContext.value || inject(topicContextKey, key => key))

    return {
        activeSection,
        activeTopic,
        defineTopicContext,
        topicContext,
    }
}