import {onMounted, onUnmounted} from 'vue'
import { useRoute, useRouter } from 'vue-router'
import useProjectStore from '@/stores/projectStore'
import useNavStore from '@/stores/navStore'
import useLoadingStore from '@/stores/loadingStore'

export function useProjectLoader(weak) {
    const route = useRoute()
    const router = useRouter()
    const projectStore = useProjectStore()
    const navStore = useNavStore()
    const loadingStore = useLoadingStore()

    return new Promise((resolve, reject) => {
        onMounted(() => {
            loadingStore.loading = true
            const projectId = route.params.id
            const targetFunction = weak ? projectStore.loadWeak : projectStore.load
            
            targetFunction(projectId)
            .then(() => {
                navStore.heading = projectStore.project.name
                resolve()
            })
            .catch(err => {
                switch (err.message) {
                    case 'No such project.':
                        router.push({name: 'NotFound'})
                        break
                    default:
                        reject(err)
                }
            })
            .finally(() => loadingStore.loading = false)
        })
        onUnmounted(() => navStore.heading = '')
    })
}
