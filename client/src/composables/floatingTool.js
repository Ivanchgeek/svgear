import { ref, onMounted, onUnmounted, readonly } from 'vue'

export function useFloatingTool(self) {

    const floatingVisible = ref(false)

    const showFloating = () => floatingVisible.value = true
    const hideFloating = () => floatingVisible.value = false
    
    function documentClick({target}) {
        if (!self.value.contains(target)) hideFloating()
    }
    
    onMounted(() => document.addEventListener('click', documentClick))
    onUnmounted(() => document.removeEventListener('click', documentClick))

    return {
        floatingVisible: readonly(floatingVisible),
        showFloating,
        hideFloating,
    }

}
