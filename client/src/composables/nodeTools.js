import { reactive } from 'vue'

export function useNodeTools() {

    const tools = reactive({
        values: {},
        context: {},
        isConnected: () => false,
    })

    function handleTools(event) {
        Object.assign(tools, event)
    }

    return {
        handleTools, 
        tools,
    }

}
