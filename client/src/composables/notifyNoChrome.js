import useDialogStore from '@/stores/dialogStore'
import NoChromeDialog from '@/components/Dialog/NoChromeDialog.vue'

const chromeAlertPreventKey = 'chrome-alert-prevent'

export function notifyNoChrome() {
    if (!(window.chrome || localStorage.getItem(chromeAlertPreventKey))) {
        const dialogStore = useDialogStore()
        dialogStore.callDialog(NoChromeDialog)
        .then(showAgain => {
            if (!showAgain) preventChromeAlert()
        })
    }
}

export function preventChromeAlert() {
    localStorage.setItem(chromeAlertPreventKey, 'prevent')
}
