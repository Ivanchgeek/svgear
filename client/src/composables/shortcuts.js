import {onMounted, onUnmounted} from 'vue'
import useProjectStore from "@/stores/projectStore";

export function useShortcuts() {
    const projectStore = useProjectStore()

    function saveProject(event) {
        if (event.key === 's' && event.ctrlKey) {
            event.preventDefault()
            try {
                projectStore.save()
            } catch(e) {
                console.log(e)
            }
            return false
        }
    }

    const activeShortcuts = [saveProject]

    onMounted(() => {
        activeShortcuts.forEach(shortcut => document.addEventListener('keydown', shortcut))
    })
    onUnmounted(() => {
        activeShortcuts.forEach(shortcut => document.removeEventListener('keydown', shortcut))
    })

}
