import { provide, inject, readonly } from "vue"

const nodeContextKey = Symbol('node context')

export function provideNodeContext(context) {
    provide(nodeContextKey, readonly(context))
}

export function useNodeContext() {
    return inject(nodeContextKey)
}
