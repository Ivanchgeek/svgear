<template>
    <section class="node-editor" :class="{maximized: nodeEditorOptions.nodesViewOptions.maximized}">
        <section 
            ref="nodesView"
            class="nodes-view"
            @mousemove="viewMouseMove"
            @mouseup="viewMouseUp"
            @mouseleave="viewMouseUp"
            @scroll="viewScroll"
            @contextmenu="handleContext"
            @wheel="viewWheel"
        >
            <div class="nodes-container" ref="nodesContainer">
                <node-connections-overlay></node-connections-overlay>
                <component
                    v-for="node in nodesStore.nodes"
                    :key="node.id" 
                    :is="node.component" 
                    :id="node.id"
                ></component>
            </div>
        </section>

        <node-editor-explorer></node-editor-explorer>
        <node-editor-size-control></node-editor-size-control>
    </section>
</template>

<script setup>
import { computed, onMounted, onUnmounted, reactive, ref, toValue, watch } from 'vue'
import { useI18n } from 'vue-i18n'
import useNodesStore from '@/stores/nodesStore'
import useNodeEditorOptions from '@/stores/nodeEditorOptionsStore'
import useNodeConnections from '@/stores/nodeConnectionsStore'
import useContextMenu from '@/stores/contextMenu'
import { useCoordinates } from '@/composables/coordinates' 
import nodeTypes from '@/utl/nodeTypes'
import NodeConnectionsOverlay from '@/components/NodeConnectionsOverlay.vue'
import NodeEditorExplorer from '@/components/NodeEditorExplorer.vue'
import NodeEditorSizeControl from '@/components/NodeEditorSizeControl.vue'

const { t } = useI18n()

const nodesStore =  useNodesStore()
const nodeEditorOptions = useNodeEditorOptions()
const nodeConnections = useNodeConnections()
const contextMenuStore = useContextMenu()

const convertedContainerSize = computed(() => {
    return {
        width: nodeEditorOptions.nodesContainerOptions.width + 'px',
        height: nodeEditorOptions.nodesContainerOptions.height + 'px',
    }
})

const nodesView = ref(null)
const nodesContainer = ref(null)

function updateViewSize() {
    const nodesViewVal = toValue(nodesView)
    nodeEditorOptions.bindNodesViewSize(nodesViewVal.clientWidth, nodesViewVal.clientHeight)
}
onMounted(() => {
    updateViewSize()
    window.addEventListener('resize', updateViewSize)
})
onUnmounted(() => {
    window.removeEventListener('resize', updateViewSize)
})
watch(() => nodeEditorOptions.nodesViewOptions.maximized, updateViewSize, {flush: 'post'})

function centerNodesView() {
    const nodesViewVal = toValue(nodesView)
    const nodesContainerVal = toValue(nodesContainer)
    if (nodesViewVal && nodesContainerVal) {
        nodesViewVal.scrollTo(
            (nodesContainerVal.scrollWidth - nodeEditorOptions.nodesViewOptions.width) / 2, 
            (nodesContainerVal.scrollHeight - nodeEditorOptions.nodesViewOptions.height) / 2
        )
    }
}
onMounted(centerNodesView)

watch(() => nodeEditorOptions.nodesViewOptions.scrollRequest, () => {
    const nodesViewVal = toValue(nodesView)
    const nodesContainerVal = toValue(nodesContainer)
    const request = toValue(nodeEditorOptions.nodesViewOptions.scrollRequest)
    nodesViewVal.scrollTo({
        top: (nodesContainerVal.scrollHeight - nodeEditorOptions.nodesViewOptions.height) / 2 + request.y,
        left: (nodesContainerVal.scrollWidth - nodeEditorOptions.nodesViewOptions.width) / 2 + request.x, 
        behavior: 'smooth',
    })
})

const { getDocumentCoordinates } = useCoordinates()
function viewMouseMove({clientX, clientY}) {
    const documentPos = getDocumentCoordinates(nodesView)
    const nodesViewVal = toValue(nodesView)
    const scale = nodeEditorOptions.nodesViewOptions.scale / 100 
    nodeEditorOptions.updateContainerCursor(
        clientX / scale + nodesViewVal.scrollLeft - nodeEditorOptions.nodesContainerOptions.width / 2 - documentPos.x,
        clientY / scale + nodesViewVal.scrollTop - nodeEditorOptions.nodesContainerOptions.height / 2 - documentPos.y
    )

    nodeMover()
}

const lastScroll = reactive({})

function updateLastScroll() {
    const nodesViewVal = toValue(nodesView)
    Object.assign(lastScroll, {
        left: nodesViewVal.scrollLeft,
        top: nodesViewVal.scrollTop,
    })
}
onMounted(updateLastScroll)

function viewScroll() {
    const nodesViewVal = toValue(nodesView)
    nodeEditorOptions.updateContainerCursor(
        nodeEditorOptions.nodesContainerOptions.cursor.x + nodesViewVal.scrollLeft - lastScroll.left,
        nodeEditorOptions.nodesContainerOptions.cursor.y + nodesViewVal.scrollTop - lastScroll.top,
    )
    updateLastScroll()

    nodeMover()
}

function nodeMover() {
    if (!nodesStore.nodeGrabbed) return
    const grabbed = nodesStore.grabbedNode
    nodesStore.moveNodeTo(
        grabbed.id,
        nodeEditorOptions.nodesContainerOptions.cursor.x + grabbed.offsetX,
        nodeEditorOptions.nodesContainerOptions.cursor.y + grabbed.offsetY,
    )
}

function viewMouseUp() {
    nodesStore.dropNode()
    nodeConnections.finishConnection()
}

const zoomPercentage = computed(() => `${nodeEditorOptions.nodesViewOptions.scale}%`)

function viewWheel(event) {
    if (event.ctrlKey) {
        event.preventDefault()
        const max = nodeEditorOptions.nodesViewOptions.scaleMax
        const min = nodeEditorOptions.nodesViewOptions.scaleMin
        const delta = Math.round(-event.deltaY / 50)
        const update = nodeEditorOptions.nodesViewOptions.scale + delta

        if (update > max) {
            nodeEditorOptions.nodesViewOptions.scale = max
            return
        }
        if (update < min) {
            nodeEditorOptions.nodesViewOptions.scale = min
            return
        }
        nodeEditorOptions.nodesViewOptions.scale = update
    }
}

function handleContext() {

    function nodeCreator(type) {
        const position = {...nodeEditorOptions.nodesContainerOptions.cursor}
        return () => {
            nodesStore.createNode(type, position)
        }
    }

    contextMenuStore.addPoint({
        text: t('IDE.context.addNode'),
        children: [
            {text: 'Blend', click: nodeCreator(nodeTypes.feBlend)},
            {text: 'Color Matrix', click: nodeCreator(nodeTypes.feColorMatrix)},
            {text: 'Composite', click: nodeCreator(nodeTypes.feComposite)},
            {text: 'Convolve Matrix', click: nodeCreator(nodeTypes.feConvolveMatrix)},
            {text: 'Displacement Map', click: nodeCreator(nodeTypes.feDisplacementMap)},
            {text: 'Flood', click: nodeCreator(nodeTypes.feFlood)},
            {text: 'Gaussian Blur', click: nodeCreator(nodeTypes.feGaussianBlur)},
            {text: 'Image', click: nodeCreator(nodeTypes.feImage)},
            {text: 'Merge', click: nodeCreator(nodeTypes.feMerge)},
            {text: 'Morphology', click: nodeCreator(nodeTypes.feMorphology)},
            {text: 'Offset', click: nodeCreator(nodeTypes.feOffset)},
            {text: 'Tile', click: nodeCreator(nodeTypes.feTile)},
            {text: 'Turbulence', click: nodeCreator(nodeTypes.feTurbulence)},
            {
                text: 'Component Transfer',
                children: [
                    {text: 'Base', click: nodeCreator(nodeTypes.feComponentTransfer)},
                    {text: 'Channel', click: nodeCreator(nodeTypes.feComponentTransferChannel)},
                ]
            },
            {
                text: 'Light',
                children: [
                    {text: 'Diffuse Lighting', click: nodeCreator(nodeTypes.feDiffuseLighting)},
                    {text: 'Specular Lighting', click: nodeCreator(nodeTypes.feSpecularLighting)},
                    {text: 'Distant Light', click: nodeCreator(nodeTypes.feDistantLight)},
                    {text: 'Point Light', click: nodeCreator(nodeTypes.fePointLight)},
                    {text: 'Spot Light', click: nodeCreator(nodeTypes.feSpotLight)},
                ]
            },
        ]
    })
}

</script>

<style lang="less" scoped>
@import '@/styles/bundle.less';

.node-editor {
    position: relative;
    height: calc((100vh - @navHeight) * 0.6);
    width: 100%;
    overflow: hidden;

    &.maximized {
        height: calc((100vh - @navHeight) * 0.99);
    }

    .nodes-view {
        .rounded();
        .inner-shadow(@background-2);
        height: 100%;
        width: 100%;
        overflow: scroll;
        zoom: v-bind('zoomPercentage');

        .nodes-container {
            width: v-bind('convertedContainerSize.width');
            height: v-bind('convertedContainerSize.height');
            background-color: @background-2;
            background-image: url('@/assets/node-editor-bg.png');
            background-size: 1.2rem;
            position: relative;
        }

    }
}
</style>
